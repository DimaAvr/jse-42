package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.dto.AbstractOwnerEntityDTO;

import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntityDTO> extends IService<E> {

    E add(@NotNull String userId, @Nullable final E entity);

    void remove(@NotNull String userId, @Nullable final E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    void clear(@Nullable String userId);

    @NotNull
    E findById(@Nullable String userId, @Nullable final String id);

    @NotNull
    void removeById(@Nullable String userId, @Nullable final String id);

}

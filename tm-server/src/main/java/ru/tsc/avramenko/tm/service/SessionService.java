package ru.tsc.avramenko.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.ISessionRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.util.HashUtil;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.connectionService = connectionService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(serviceLocator.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }


    @Override
    @SneakyThrows
    public void close(@NotNull SessionDTO session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            validate(session);
            repository.remove(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public SessionDTO open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO resultSession = sign(session);
            repository.add(session);
            sqlSession.commit();
            return resultSession;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull SessionDTO session, @Nullable Role role) throws AccessDeniedException{
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable SessionDTO session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            return sessionRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

}
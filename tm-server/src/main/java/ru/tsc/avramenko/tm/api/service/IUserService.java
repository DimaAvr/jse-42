package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.UserDTO;
import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void clear();

    void addAll(@Nullable List<UserDTO> users);

    @Nullable
    List<UserDTO> findAll();

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO setRole(@Nullable String id, @Nullable Role role);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    boolean isUserExist(@NotNull String id);

    @NotNull
    UserDTO updateUserById(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    UserDTO updateUserByLogin(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName, @Nullable String email);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

}
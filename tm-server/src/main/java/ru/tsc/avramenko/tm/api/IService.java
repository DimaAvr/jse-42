package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.dto.AbstractEntityDTO;

public interface IService <E extends AbstractEntityDTO> extends IRepository<E> {
}
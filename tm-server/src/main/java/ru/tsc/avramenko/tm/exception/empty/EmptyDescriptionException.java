package ru.tsc.avramenko.tm.exception.empty;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty.");
    }

}

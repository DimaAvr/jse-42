package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<TaskDTO> findTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @NotNull
    TaskDTO bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    TaskDTO unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    @Nullable
    void removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    void removeProjectByName(@NotNull String userId, @Nullable String name);

}
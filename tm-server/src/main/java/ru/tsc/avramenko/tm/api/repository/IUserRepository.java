package ru.tsc.avramenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Select("SELECT * FROM `app_user` WHERE `login` = #{login} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "locked", property = "locked")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    UserDTO findByLogin(final String login);

    @Select("SELECT * FROM `app_user` WHERE `email` = #{email} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "locked", property = "locked")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    UserDTO findByEmail(final String email);

    @Select("SELECT * FROM `app_user` WHERE `id` = #{id} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "email", property = "email")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "locked", property = "locked")
    @Result(column = "login", property = "login")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "role", property = "role")
    UserDTO findById(final String id);

    @Nullable
    @Delete("DELETE FROM `app_user` WHERE `login` = #{login}")
    void removeUserByLogin(@NotNull String login);

    @Nullable
    @Delete("DELETE FROM `app_user` WHERE `id` = #{id}")
    void removeUserById(@NotNull String id);

    @Insert("INSERT INTO `app_user` " +
            "(`id`, `email`, `login`, `role`, `locked`, `first_name`, `last_name`, `middle_name`, `password_hash`) " +
            "VALUES(#{id},#{email},#{login},#{role},#{locked}," +
            "#{firstName},#{lastName},#{middleName},#{passwordHash})")
    void add(@NotNull UserDTO user);

    @Update("UPDATE `app_user` " +
            "SET `email`=#{email}, `login`=#{login}, `role`=#{role}, `password_hash`=#{passwordHash}, `locked`=#{locked}, " +
            "`first_name`=#{firstName}, `last_name`=#{lastName}, `middle_name`=#{middleName} WHERE `id`=#{id}")
    void update(@NotNull UserDTO user);

    @Delete("DELETE FROM `app_user`")
    void clear();

    @Select("SELECT * FROM `app_user`")
    @Result(column = "id", property = "id")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Result(column = "role", property = "role")
    @Result(column = "locked", property = "locked")
    @Result(column = "login", property = "login")
    @Result(column = "email", property = "email")
    @Result(column = "password_hash", property = "passwordHash")
    List<UserDTO> findAll();

}
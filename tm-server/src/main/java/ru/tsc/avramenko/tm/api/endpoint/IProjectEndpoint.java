package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    void createProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    );

    @Nullable
    @WebMethod
    List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    void clearProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    ProjectDTO finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    ProjectDTO finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    ProjectDTO finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void setProjectStatusById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    void setProjectStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @WebMethod
    ProjectDTO startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    ProjectDTO startProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @WebMethod
    ProjectDTO startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @WebMethod
    ProjectDTO updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

}
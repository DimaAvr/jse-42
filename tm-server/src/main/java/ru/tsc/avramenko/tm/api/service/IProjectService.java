package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    ProjectDTO changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    ProjectDTO findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO startById(@Nullable String userId, @Nullable String id);

    @NotNull
    ProjectDTO startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    ProjectDTO finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    ProjectDTO finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO finishByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll();

    void clear(@Nullable String userId);

    void clear();

    @Nullable
    ProjectDTO findById(@Nullable String userId, @Nullable String id);

    void addAll(@Nullable List<ProjectDTO> projects);

}
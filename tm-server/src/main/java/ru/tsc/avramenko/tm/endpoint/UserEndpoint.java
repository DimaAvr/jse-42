package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.endpoint.IUserEndpoint;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isUserExist(id);
    }

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        return serviceLocator.getUserService().isEmailExist(email);
    }

    @Override
    @Nullable
    @WebMethod
    public boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        return serviceLocator.getUserService().isLoginExist(login);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO registryUser(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getUserService().create(login, password, email);
        return serviceLocator.getSessionService().open(login, password);
    }

}
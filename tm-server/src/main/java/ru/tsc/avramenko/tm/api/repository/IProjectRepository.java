package ru.tsc.avramenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import java.util.List;

public interface IProjectRepository {

    @Select("SELECT * FROM `app_project` WHERE `name`=#{name} AND `user_id`=#{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    ProjectDTO findByName(@Param("userId") final String userId, @Param("name") final String name);

    @Select("SELECT * FROM `app_project` WHERE `user_id`=#{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    ProjectDTO findByIndex(@Param("userId") final String userId, @Param("index") final int index);

    @Delete("DELETE FROM `app_project` WHERE `id` = #{projectId} AND `user_id` = #{userId}")
    void removeById(@Param("userId") final String userId, @Param("projectId") final String projectId);

    @Delete("DELETE FROM `app_project` WHERE `name` = #{name} AND `user_id` = #{userId}")
    void removeByName(@Param("userId") final String userId, @Param("name")final String name);

    @Delete("DELETE FROM `app_project` WHERE `user_id` = #{userId} OFFSET #{index}")
    void removeByIndex(@Param("userId") final String userId, @Param("index") final int index);

    @Update("UPDATE `app_project` " +
            "SET `name`=#{name}, `description`=#{description}, `status`=#{status}, `start_date`=#{startDate}, " +
            "`finish_date`=#{finishDate}, `created_date`=#{created}, `user_id`=#{userId} WHERE `id` = #{id}")
    void update(@NotNull ProjectDTO project);

    @Insert("INSERT INTO `app_project` " +
            "(`id`, `name`, `description`, `status`, `start_date`, `finish_date`, `created_date`, `user_id`)" +
            "VALUES(#{id},#{name},#{description},#{status},#{startDate},#{finishDate},#{created},#{userId})")
    void add(@NotNull ProjectDTO project);

    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} AND `user_id` = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    ProjectDTO findById(@Param("userId") final String userId, @Param("id") final String id);

    @Delete("DELETE FROM `app_project` WHERE `user_id` = #{userId}")
    void clear(final String userId);

    @Delete("DELETE FROM `app_project`")
    void clearAll();

    @Select("SELECT * FROM `app_project` WHERE `user_id` = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    List<ProjectDTO> findAllById(final String userId);

    @Select("SELECT * FROM `app_project`")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    List<ProjectDTO> findAll();

}